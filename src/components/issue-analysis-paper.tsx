import { DataGrid, GridSelectionModel, GridColDef, GridRowsProp } from '@mui/x-data-grid';
import Paper from '@mui/material/Paper';
import { SyntheticEvent, useEffect, useState } from 'react';
import axios from 'axios';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import ToggleButton from '@mui/material/ToggleButton';
import Select , { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { format } from 'date-fns';

export default function IssueAnalysisPaper(){

    const [date, setDate] = useState<Date>(new Date());
    const [type, setType] = useState<string>('other');
    const [reviewedState, setReviewedState] = useState<string>('no');
    const [issueList, setIssueList] = useState([{datePosted:'', dateIssue:'', location:'', type:'', description:'', reviewed:false, highlighted:false}])
    const [filterState, setFilterState] = useState<string>('none');
    const [refreshState, setRefreshState] = useState<boolean>(true);
    const [selectionModel, setSelectionModel] = useState<GridSelectionModel>([]);

    async function getIssueList(){
        let url = 'http://34.75.78.53/issues';
        if(filterState === 'type')
            url += `?type=${type}`;
        else if(filterState === 'datePosted')
            url += `?dateposted=${format(date, 'MM-dd-yyyy')}`;
        else if(filterState === 'dateIssue')
            url += `?dateofissue=${format(date, 'MM-dd-yyyy')}`;
        else if(filterState === 'reviewed'){
            if(reviewedState === 'yes')
                url += '?reviewed=true';
            else
                url += '?reviewed=false';
        }
        else if(filterState === 'highlighted')
            url += '?highlighted=true'; 
        const response = await axios.get(url);
        if(response.data[0].length === 0)
            setIssueList([{datePosted:'', dateIssue:'', location:'', type:'', description:'', reviewed:false, highlighted:false}])
        else
            setIssueList(response.data[0]);
    }

    const columns:GridColDef[] = [{field: 'id', hide: true},
                    {field:'datePosted', headerName:'Date Posted', minWidth:160},
                    {field:'dateIssue', headerName:'Date of Issue', minWidth:170},
                    {field:'location', headerName:'Location', minWidth:140},
                    {field:'type', headerName:'Type', minWidth:110},
                    {field:'description', headerName:'Description', minWidth:150, flex:1},
                    {field:'reviewed', hide:true},
                    {field:'highlighted', hide:true}
    ]

    const rows:GridRowsProp = issueList.map((x, i) => {
        return({id:i, datePosted:x.datePosted.replaceAll('-', '/'), dateIssue:x.dateIssue.replaceAll('-', '/'), location:x.location, type:x.type, description:x.description, reviewed:x.reviewed, highlighted:x.highlighted});
    })

    function markAsReviewed(event:SyntheticEvent){
        selectionModel.forEach( async (i) => await axios.patch('http://34.75.78.53/issues/reviewed', issueList[Number(i)]));
        setRefreshState(!refreshState);
    }

    function highlight(event:SyntheticEvent){
        selectionModel.forEach( async (i) => await axios.patch('http://34.75.78.53/issues/highlight', issueList[Number(i)]));
        setRefreshState(!refreshState);
    }

    function unhighlight(event:SyntheticEvent){
        selectionModel.forEach( async (i) => await axios.patch('http://34.75.78.53/issues/de-highlight', issueList[Number(i)]));
        setRefreshState(!refreshState);
    }

    function refresh(event:SyntheticEvent){
        setRefreshState(!refreshState);
    }

    function handleFilterState(event:SyntheticEvent, newFilterState:string){
        setFilterState(newFilterState);
    }

    const handleTypeChange = (event: SelectChangeEvent) => {
        setType(event.target.value as string);
    };

    const handleReviewedStateChange = (event: SelectChangeEvent) => {
        setReviewedState(event.target.value as string);
    };

    const handleDateChange = (newDate: Date | null) => {
        setDate(newDate ?? new Date());
    };

    useEffect(()=>{
        getIssueList();
    }, [refreshState])

    return(<Paper elevation={2}
            sx={{
                '& .MuiTypography-root': {fontFamily: "fontFamily"},
                display: 'flex',
                justifyContent: 'center',
                fontFamily: "fontFamily",
                bgcolor: "background.paper",
                height:600
            }}
    >
        <Box height="95%" width="95%" sx={{'& .MuiButton-root': { m: 1 }}}>
        <br/>
        <Typography variant='h5'>Issue Analysis Chart</Typography>
        <ToggleButtonGroup
            value={filterState}
            exclusive
            onChange={handleFilterState}
            aria-label="filter type"
            color='primary'
            sx={{'& .MuiToggleButton-root': { m: 1 }}}
        >
            <b>Filter: </b>
            <ToggleButton value="none" aria-label="no filter">None</ToggleButton>
            <ToggleButton value="type" aria-label="filter by type">Type</ToggleButton>
            <ToggleButton value="datePosted" aria-label="filter by date posted">Date Posted</ToggleButton>
            <ToggleButton value="dateIssue" aria-label="filter by date of issue">Date of Issue</ToggleButton>
            <ToggleButton value="reviewed" aria-label="filter reviewed entries">Reviewed</ToggleButton>
            <ToggleButton value="highlighted" aria-label="filter highlighted entries">Highlighted</ToggleButton>
        </ToggleButtonGroup>
        {filterState === 'type' ?
            <Select
            labelId="issue-type-label"
            id="issue-type"
            value={type}
            label="Type of Issue"
            onChange={handleTypeChange}
            >
                <MenuItem value={'infrastructure'}>Infrastructure</MenuItem><br/>
                <MenuItem value={'safety'}>Safety</MenuItem><br/>
                <MenuItem value={'public_health'}>Public Health</MenuItem><br/>
                <MenuItem value={'pollution'}>Pollution</MenuItem><br/>
                <MenuItem value={'noise'}>Noise/Disturbing the peace</MenuItem><br/>
                <MenuItem value={'other'}>Other</MenuItem>
            </Select> : (filterState === "datePosted" || filterState === "dateIssue" ? 
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                    label="Date"
                    inputFormat="MM/dd/yyyy"
                    value={date}
                    onChange={handleDateChange}
                    renderInput={(params) => <TextField {...params} />}
                    />
                </LocalizationProvider> : (filterState === 'reviewed' ? 
                    <Select
                    labelId="reviewed-label"
                    id="reviewed"
                    value={reviewedState}
                    label="Reviewed?"
                    onChange={handleReviewedStateChange}
                    >
                        <MenuItem value={'yes'}>Yes</MenuItem><br/>
                        <MenuItem value={'no'}>No</MenuItem>
                    </Select> : ''))
        }
        <IconButton aria-label='filter' onClick={refresh}><FilterAltIcon/></IconButton>
        <Box height="75%">
        <DataGrid rows={rows} 
            columns={columns}
            autoPageSize
            checkboxSelection={true}
            disableSelectionOnClick
            onSelectionModelChange={(newModel) => {setSelectionModel(newModel)}}
            selectionModel={selectionModel} /></Box>
        <Button variant='contained' color='primary' onClick={markAsReviewed}>Mark as Reviewed</Button>
        <Button variant='contained' color='primary' onClick={highlight}>Highlight</Button>
        <Button variant='contained' color='primary' onClick={unhighlight}>Unhighlight</Button>
        </Box>
    </Paper>)
}